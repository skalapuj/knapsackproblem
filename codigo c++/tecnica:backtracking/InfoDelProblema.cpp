#include "InfoDelProblema.h"
#include <iostream>


InfoDelProblema::InfoDelProblema(){
    int CapacidadDeLaMochila, CantidadDeElementos;
    cin>>CantidadDeElementos;
    cin>> CapacidadDeLaMochila;
    _CantidadDeElementos=CantidadDeElementos;
    _CapacidadDeLaMochila=CapacidadDeLaMochila;
    for (int i = 0; i < CantidadDeElementos; ++i) {
        Elemento NuevoElemento;
        _LosElementos.push_back(NuevoElemento);
    }
    _ElIteradorAlElementoSiguiente=CantidadDeElementos-1;

}
int InfoDelProblema::CantidadDeElementos() {
    return _CantidadDeElementos;

}
int InfoDelProblema::CapacidadDeLaMochila() {
    return _CapacidadDeLaMochila;
}
bool InfoDelProblema::HayMasElementos() {
    return _LosElementos.size()!=0;
}
int InfoDelProblema::ElIteradorAlElementoSiguiente(){
    return _ElIteradorAlElementoSiguiente;
}
void InfoDelProblema::LosElementosSinEse() {
    _LosElementos.pop_back();
    _ElIteradorAlElementoSiguiente--;
}
Elemento InfoDelProblema::ElementoSiguiente() {

    return _LosElementos[_LosElementos.size()-1];
}
Elemento InfoDelProblema::ElementoIesimo(int i) {
    return _LosElementos[i];
}
void InfoDelProblema::DisminuirLaCapacidad(int valor) {
    _CapacidadDeLaMochila=_CapacidadDeLaMochila-valor;
}
int InfoDelProblema::CantidadDeElementosQueQuedan() {
    return _LosElementos.size();
}
