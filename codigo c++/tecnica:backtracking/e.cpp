#include <iostream>
#include <chrono>
#include <vector>
#include "InfoDelProblema.h"
#include <math.h>
using namespace std;

int BacktrackingAuxiliar(InfoDelProblema ArmandoMochila, int Capacidad, int& BeneficioMax, int BeneficioLocal) {
    if (Capacidad < 0) {return -1;}
    if (!ArmandoMochila.HayMasElementos()){return BeneficioLocal;
    }else{
        int BeneficioDePonerTodosLosQueQuedan=0;
        for (int i = ArmandoMochila.ElIteradorAlElementoSiguiente(); i > -1; --i) {
            if(ArmandoMochila.ElementoIesimo(i).valor()<=Capacidad){ //Cuento el beneficio solo de los que entran en la mochila
                BeneficioDePonerTodosLosQueQuedan = BeneficioDePonerTodosLosQueQuedan + ArmandoMochila.ElementoIesimo(i).beneficio();
            }
        }
        if (BeneficioMax < BeneficioLocal + BeneficioDePonerTodosLosQueQuedan) { //Me fijo si tiene sentido recorrer esa rama
            if (ArmandoMochila.ElementoSiguiente().valor()<=Capacidad) {
                int localConEse = BeneficioLocal + ArmandoMochila.ElementoSiguiente().beneficio();
                int localSinEse = BeneficioLocal;
                int nuevaCapacidad = Capacidad - ArmandoMochila.ElementoSiguiente().valor();
                ArmandoMochila.LosElementosSinEse();
                int BacktrackinConEse = BacktrackingAuxiliar(ArmandoMochila, nuevaCapacidad, BeneficioMax, localConEse);
                int BacktrackingSinEse = BacktrackingAuxiliar(ArmandoMochila, Capacidad, BeneficioMax,localSinEse);
                BeneficioLocal = max(BacktrackinConEse, BacktrackingSinEse);
            } else {
                ArmandoMochila.LosElementosSinEse();
                BeneficioLocal = BacktrackingAuxiliar(ArmandoMochila, Capacidad, BeneficioMax, BeneficioLocal);
            }
        }
        if(BeneficioMax<BeneficioLocal){
            BeneficioMax=BeneficioLocal;
        }


    }
    return BeneficioMax;
}


int main() {
    InfoDelProblema ArmandoLaMochila;
    int BenedicioMaximoHastaElMomento=0;
    int BeneficioLocal=0;
    int suma = 0;
    int i = 50;
    int Cantidad= ArmandoLaMochila.CantidadDeElementos();
    int capaciadad= ArmandoLaMochila.CapacidadDeLaMochila();
    while (i > 0) {
        auto start = chrono::steady_clock::now();
        BacktrackingAuxiliar(ArmandoLaMochila,ArmandoLaMochila.CapacidadDeLaMochila(),BenedicioMaximoHastaElMomento,BeneficioLocal);
        auto end = chrono::steady_clock::now();
        suma = suma + chrono::duration_cast<chrono::nanoseconds>(end - start).count();
        i--;
    }
    cout << Cantidad << ',' << capaciadad << ',' << pow(2, Cantidad) * pow(Cantidad,2) << ',' << "BT" << endl;
    return 0;
}
