#include <iostream>
#include <chrono>
#include <vector>
#include "InfoDelProblema.h"
using namespace std;


int ProgramacionDinamicaAuxiliar(vector<vector<int>> &matrizDeResultados, int capacidad, Elemento e, InfoDelProblema ArmarMochila) {
    if (matrizDeResultados[capacidad+1][ArmarMochila.ElIteradorAlElementoSiguiente()+1] == -1) {
        if (e.valor() <= capacidad) {
            int nuevaCapacidad = capacidad - e.valor();
            int elBeneficio = e.beneficio();
            ArmarMochila.LosElementosSinEse();
            int sinEse = ProgramacionDinamicaAuxiliar(matrizDeResultados, capacidad, ArmarMochila.ElementoSiguiente(), ArmarMochila);
            int conEse = ProgramacionDinamicaAuxiliar(matrizDeResultados, nuevaCapacidad, ArmarMochila.ElementoSiguiente(), ArmarMochila) + elBeneficio;
            matrizDeResultados[capacidad+1][ArmarMochila.ElIteradorAlElementoSiguiente()+1] = max(conEse, sinEse);
            } else {
                ArmarMochila.LosElementosSinEse();
                matrizDeResultados[capacidad+1][ArmarMochila.ElIteradorAlElementoSiguiente()+1] = ProgramacionDinamicaAuxiliar(matrizDeResultados, capacidad, ArmarMochila.ElementoSiguiente(),ArmarMochila);
            }
        }
        else{
            return matrizDeResultados[capacidad+1][ArmarMochila.ElIteradorAlElementoSiguiente()];
        }
        return matrizDeResultados[capacidad+1][ArmarMochila.ElIteradorAlElementoSiguiente()+1];
}

int ProgramacionDinamica(){
    InfoDelProblema ArmarMochila;
    vector<int> laFila(ArmarMochila.CantidadDeElementos() + 1,-1);
    vector<vector<int>> matrizDeResultadoWporN(ArmarMochila.CapacidadDeLaMochila() + 2);
    for (int i = 0; i <matrizDeResultadoWporN.size(); ++i) {
        matrizDeResultadoWporN[i]=laFila;
        }
    return ProgramacionDinamicaAuxiliar(matrizDeResultadoWporN,ArmarMochila.CapacidadDeLaMochila(),ArmarMochila.ElementoSiguiente(),ArmarMochila);
}

int main() {
    InfoDelProblema ArmarMochila;
    vector<int> laFila(ArmarMochila.CantidadDeElementos() + 1,-1);
    vector<vector<int>> matrizDeResultadoWporN(ArmarMochila.CapacidadDeLaMochila() + 2, laFila);
    vector<int> laFilaDeCero(ArmarMochila.CantidadDeElementos() + 1,0);
    matrizDeResultadoWporN[0]=laFilaDeCero;
    for (int i = 0; i < matrizDeResultadoWporN.size() ; ++i) {
        matrizDeResultadoWporN[i][0]=0;
    }
    /*for (int i = 0; i <matrizDeResultadoWporN.size(); ++i) {
        matrizDeResultadoWporN[i]=laFila;
        }*/
    int suma = ProgramacionDinamicaAuxiliar(matrizDeResultadoWporN,ArmarMochila.CapacidadDeLaMochila(),ArmarMochila.ElementoSiguiente(),ArmarMochila);
    /*int i = 50;
    while (i > 0) {
        auto start = chrono::steady_clock::now();
        ProgramacionDinamicaAuxiliar(matrizDeResultadoWporN,ArmarMochila.CapacidadDeLaMochila(),ArmarMochila.ElementoSiguiente(),ArmarMochila);
        auto end = chrono::steady_clock::now();
        suma = suma + chrono::duration_cast<chrono::nanoseconds>(end - start).count();
        i--;
    }*/
    cout << suma << endl;
    return 0;
}
