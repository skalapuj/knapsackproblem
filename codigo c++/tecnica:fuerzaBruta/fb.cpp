#include <iostream>
#include <chrono>
#include <vector>
#include "InfoDelProblema.h"
using namespace std;


int FuerzaBrutaAuxiliar(InfoDelProblema ArmarMochila, int& laSolucion, int capacidad){
    if (ArmarMochila.Elementos().size()==0){return laSolucion;}
    if (ArmarMochila.ElementoSiguiente().valor()<= capacidad){
        int laSolucionConEse = laSolucion + ArmarMochila.ElementoSiguiente().beneficio();
        int elValorDeEse = ArmarMochila.ElementoSiguiente().valor();
        ArmarMochila.LosElementosSinEse();
        int conEse = FuerzaBrutaAuxiliar(ArmarMochila,laSolucionConEse, capacidad - elValorDeEse);
        int sinEse = FuerzaBrutaAuxiliar(ArmarMochila, laSolucion,capacidad);
        laSolucion = max(conEse,sinEse);
    }else{
        ArmarMochila.LosElementosSinEse();
        laSolucion = FuerzaBrutaAuxiliar(ArmarMochila, laSolucion,capacidad);
    }
    return laSolucion;
}
int FuerzaBruta(){
    InfoDelProblema ArmarMochila;
    int laSolucion=0;
    return FuerzaBrutaAuxiliar(ArmarMochila,laSolucion, ArmarMochila.CapacidadDeLaMochila());
}


int main() {
    InfoDelProblema ArmarMochila;
    int laSolucion = 0;
    int suma = FuerzaBrutaAuxiliar(ArmarMochila, laSolucion, ArmarMochila.CapacidadDeLaMochila());
    cout << suma << endl;
    return 0;
}
