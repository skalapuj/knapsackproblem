#include <list>
#include <ostream>
#include <vector>
#include "Elemento.h"
#ifndef INFODELPROBLEMA_H
#define INFODELPROBLEMA_H
using namespace std;

class InfoDelProblema{
private:
    int _CapacidadDeLaMochila;
    int _CantidadDeElementos;
    vector<Elemento> _LosElementos;
    int _ElIteradorAlElementoSiguiente;

public:
    InfoDelProblema();
    bool HayMasElementos();
    int CapacidadDeLaMochila();
    int CantidadDeElementos();
    Elemento ElementoSiguiente();
    void LosElementosSinEse();
    int ElIteradorAlElementoSiguiente();
    Elemento ElementoIesimo(int i);
    void DisminuirLaCapacidad(int valor);
    int CantidadDeElementosQueQuedan();



};

#endif
