
#ifndef ELEMENTO_H
#define ELEMENTO_H

class Elemento{
private:
    int _valor;
    int _beneficio;
public:
    Elemento();
    int valor();
    int beneficio();
};

#endif
